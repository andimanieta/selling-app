const express = require("express");
const bodyParser = require('body-parser');
const mongoose = require('mongoose')

const app = express();
mongoose.connect('mongodb+srv://andi:iso001@cluster0.gbl8p.mongodb.net/sellingappDB?retryWrites=true&w=majority',
    { useNewUrlParser: true,
      useUnifiedTopology: true })
    .then(() => console.log('Connexion à MongoDB réussie !'))
    .catch(() => console.log('Connexion à MongoDB échouée !'));

app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content, Accept, Content-Type, Authorization');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
  next();
});

app.use(bodyParser.json());

app.post('/api/stuff', (req, res, next) => {
  res.status(200);
  console.log(req.body);
  next();
});

app.use('/api/stuff', (req, res) => {
  const stuff = [
    {
      _id:'dsfjmqdklsfj',
      title: 'Bike',
      description: 'Description of bike',
      imageUrl: 'http://tvsudprovence.fr/wp-content/uploads/2016/08/comp' +
          'aratif-velo-pliant.jpg',
      userId: 'fdsqklmdjsf'
    },

    {
      _id:'druejfchkfd',
      title: 'Computer',
      description: 'Description of computer',
      imageUrl: 'https://ventes-pas-cher.com/images/stories/11743531466782.jpg',
      userId: 'dfsqggrerf'
    }
  ];
  res.status(200).json(stuff);
});

module.exports = app;
